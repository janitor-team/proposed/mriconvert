///
/**
    */

#ifndef GE_EPI_HANDLER_H
#define GE_EPI_HANDLER_H

#include "SeriesHandler.h"

namespace jcs {
  ///
  /**
    */
  class GeEpiHandler : public SeriesHandler
  {
  public:
    GeEpiHandler(const std::string& seriesUid);

  protected:
    virtual VolListType ReadVolIds(DicomFile& file);

  };
}

#endif
