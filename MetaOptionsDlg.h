/// MetaOptionsDlg.h
/**
  */

#ifndef META_OPTIONS_DLG_H_
#define META_OPTIONS_DLG_H_

#include "BasicOptionsDlg.h"

namespace jcs {

  class NewMetaOutputter;
  
  class MetaOptionsDlg : public BasicOptionsDlg
  {
  public:
    MetaOptionsDlg(NewMetaOutputter* outputter);

    void SetFields(std::vector<std::string> fields);
    std::vector<std::string> GetFields() const ;

    void OnOkay(wxCommandEvent& event);
    void OnClear(wxCommandEvent& event);

    bool NeedsRebuild() const { return mNeedsRebuild; }
    bool SaveHeaderOnly() const { return mpHeaderOnlyCheck->GetValue(); }

  protected:
    DECLARE_EVENT_TABLE()

      private:
    wxCheckBox* mpHeaderOnlyCheck;
    wxCheckListBox* mpCheckList;

    int mAddTag(const std::string tag);
    int mCheckItem(const std::string item);

    bool mNeedsRebuild;
    bool mSplit;
    int mDim;
    bool mHeaderOnly;
    int mSkip;

    NewMetaOutputter* mOutputter;

  };

}

#endif
