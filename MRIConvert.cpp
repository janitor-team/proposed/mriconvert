/// MRIConvert.cpp
/**
*/

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------
// For compilers that support precompilation, includes "wx/wx.h".
#if defined(__WXGTK__) || defined(__WXMOTIF__)
  #include <wx/wx.h>
#endif

#include <iostream>

#include <wx/wxprec.h>
#include <wx/config.h>

#include "MRIConvert.h"
#include "McFrame.h"
#include "resource.h"
#include "StringConvert.h"
#include "Globals.h"
#include "ConfigValues.h"

// Create a new application object: this macro will allow wxWindows to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also declares the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MRIConvert and
// not wxApp)
IMPLEMENT_APP(MRIConvert)


// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

// 'Main program' equivalent: the program execution "starts" here
bool MRIConvert::OnInit()
{
  jcs::verbose = false;

  // Start with loading preferences for window size and position.
  wxSize size;
  long minWidth = 400, minHeight = 400, maxWidth = 10000, maxHeight = 10000;

  // Constrain width and height of McFrame to avoid assert failure undoer OS X:
  // Assertion failed: (winRgnBounds.size.width <= 1000000),
  // function CGSNewWindowWithOpaqueShape,
  // file Services/Windows/CGSWindow.c, line 572.
  size.SetWidth(std::min(wxConfig::Get()->Read(CFG_framewidth, minWidth), maxWidth));
  size.SetHeight(std::min(wxConfig::Get()->Read(CFG_frameheight, minHeight), maxHeight));

  // 20130326cdt Do we want to save and restore window location?
  wxPoint pos;
  wxConfig::Get()->Read(CFG_frameposx, &pos.x);
  wxConfig::Get()->Read(CFG_frameposy, &pos.y);

  // For now, let's rely on wxWisdom for positioning.
  pos = wxDefaultPosition;

  // Localization steps
  if ( !m_locale.Init() )
    {
      wxLogError(_T("This language is not supported by the system."));
    }
  // Development only, distribution should install in standard location.
  //wxLocale::AddCatalogLookupPathPrefix(_T("l10n"));
  // Initialize the catalogs we'll be using
  m_locale.AddCatalog(_T("mriconvert"));

  // create the main application window
  McFrame *frame = new McFrame(_T("MRIConvert"), pos, size);
  wxTheApp->SetAppName(_T("MRIConvert"));

  // and show it (the frames, unlike simple controls, are not shown when
  // created initially)
  frame->Show(TRUE);

  // success: wxApp::OnRun() will be called which will enter the main message
  // loop and the application will run. If we returned FALSE here, the
  // application would exit immediately.
  return TRUE;
}
