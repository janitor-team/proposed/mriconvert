#ifndef BV_FILES_H_
#define BV_FILES_H_

#include <fstream>
#include <vector>

#include <wx/defs.h>

#include "Globals.h"

namespace jcs {

struct FmrHeader {
	int volumes;
	int slices;
	int skipVolumes;
	std::string prefix;
	int tr;
	int interSliceT;
	int columns;
	int rows;
	int layoutColumns;
	int layoutRows;
	float IpResX;
	float IpResY;
	float sliceThickness;
	float sliceGap;
};

class BvStc {

public:

	BvStc(const std::string& filename) : mFileName(filename) {}
	~BvStc();

	int	WriteStc(wxUint16 rows, wxUint16 cols, 
		std::vector<unsigned short>& data);

private :

	std::string mFileName;
	std::fstream mFile;
	
	int  mOpenFile(std::ios::openmode mode);
	void mCloseFile()	{ mFile.close(); }
};

class BvVmr {

public:

	BvVmr(const std::string& filename) : mFileName(filename) {}
	~BvVmr();

	int	WriteVmr(std::vector<wxUint16>& dim, std::vector<wxUint8>& data);
	int	WriteV16(std::vector<wxUint16>& dim, std::vector<wxInt16>& data);

private :

	std::string mFileName;
	std::fstream mFile;
	
	int  mOpenFile(std::ios::openmode mode, const char* suffix);
	void mCloseFile()	{ mFile.close(); }
};

class BvFmr {

public :

	BvFmr(const std::string& filename) : mFileName(filename) {}
	~BvFmr();

	void WriteFmr(const FmrHeader& header); 

private :

	std::string mFileName;
	std::fstream mFile;
	
	int  mOpenFile(std::ios::openmode mode);
	void mCloseFile()	{ mFile.close(); }


};

}

#endif
