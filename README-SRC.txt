Building MRIConvert/mcverter From Source
========================================
This document details the packages and configuration needed to build MRIConvert and mcverter from source.


Requirements
============
The MRIConvert source package
A C++ development system
CMake 2.8.2 or greater
wxWidgets 3.0.x


CMake
=====
The generic installation is suitable in most cases. If your platform application manager does not provide version 2.8.2 or greater, installation from source is recommended.


wxWidgets
=========
We recommend building this package from source. This must be built with Unicode support. Building as a static library is also recommended. Configuration details for different platforms are listed below.

Linux
-----
Install from the wxWidgets source package.
./configure --prefix=/usr --disable-shared --disable-compat28 --enable-utf8 --enable-intl --enable-dnd
make
make install

OS X
----
Install from the wxWidgets source package. Adjust the --with-macosx-sdk setting to match your available SDK.
./configure --prefix=/usr --disable-shared --disable-compat28 --enable-utf8 --enable-intl --enable-dnd --with-macosx-sdk=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk --with-macosx-version-min=10.9
make
make install

Windows
-------
1) Run installer wxMSW-3.0.2-Setup.exe. Recommended location is the root of the system drive. In the subesequent instructions, we assume this location is the root of C: drive.
2) Modify C:\wxWidgets-3.0.2\include\wx\msw\setup.h to set appropriate options:

#define WXWIN_COMPATIBILITY_2_6 0

// Recommended setting: 0 (please update your code)
#define WXWIN_COMPATIBILITY_2_8 0

and 

#ifndef wxUSE_UNICODE
    #define wxUSE_UNICODE 1
#endif

3) Load project C:\wxWidgets-3.0.2\build\msw\wx.sln into Visual Studio. Visual Studio may need to convert the project files.
4) Be sure Code Generation -> Runtime Library is set to Multi-threaded (/MT) for all projects, the default is Multi-threaded DLL (/MD).
5) Use Configuration Manager to change the Configuration for all sub-projects to Release.
6) Build -> Build Solution to create the libraries.


MRIConvert and mcverter
=======================
Extract the tarball to a convenient location:

tar xzf MRIConvert-n.n.n-src.tar.gz
cd MRIConvert-n.n.n
cmake
make

On Windows, cmake (or the cmake gui) may not be able to find wxWidgets. Be sure that the variables wxWidgets_ROOT_DIR and wxWidgets_LIB_DIR in CMakeLists.txt are set appropriate to your installation of wxWidgets.

To install the resulting executables for general use, copy them from the release directory to the location for your binaries, e.g. /usr/bin or /usr/local/bin.  You will need administrator privileges for this step. 

