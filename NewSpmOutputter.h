#ifndef NEW_SPM_OUTPUTTER_H_
#define NEW_SPM_OUTPUTTER_H_

#include <string>
#include <map>


#include "Basic3DOutputter.h"
#include "BasicVolumeFormat.h"

namespace jcs {

struct NewSpmHeader;

class NewSpmOutputter : public Basic3DOutputter {

public :
	NewSpmOutputter();

	virtual int ConvertSeries(SeriesHandler* handler);

protected:
	virtual BasicVolumeFormat* GetOutputVolume(const char* file);
//	virtual Basic3DHeader* GetHeader(SeriesHandler* handler);

private:
	static Options CreateOptions();

};


template <class T>
class SpmConversion: public Basic3DConversion <T> {

public:
	SpmConversion(Basic3DOutputter* outputter, SeriesHandler* handler);
	~SpmConversion();

protected:
	virtual void GetHeaderForSeries();
	virtual void CompleteHeaderForVolume(std::pair<VolId, Volume<T> > volPair);
	virtual void ProcessSlice(std::vector<T>& slice);

	NewSpmHeader* GetHeader() { return mHeader; }

private:
	NewSpmHeader* mHeader;

};

}

#endif
