 /// SeriesHandler.txx
/**
*/

#ifndef SERIES_HANDLER_TXX
#define SERIES_HANDLER_TXX

namespace jcs {

  ///
  /**
    \param e DicomElement, group and element of DICOM value to find.
    \param value Variable to hold value found.
    \return 0 if no files in mFileMap. Result of dicomFile.Find otherwise.
  */
  template <class T> int
  SeriesHandler::Find(DicomElement e, T& value) const
  {
    if (mFileMap.empty()) { return 0; }
    DicomFile dicomFile((*mFileMap.begin()).first.c_str());
    return dicomFile.Find(e, value);
  }


  ///
  /**
    \param s String reference, name of DICOM element to find.
    \param value Variable to hold value found.
    \return 0 if no files in mFileMap. Result of dicomFile.Find otherwise.
  */
  template <class T> int
  SeriesHandler::Find(const std::string& s, T& value) const
  {
    if (mFileMap.empty()) { return 0; }
    DicomFile dicomFile((*mFileMap.begin()).first.c_str());
    return dicomFile.Find(s, value);
  }


  ///
  /**
    \param s String reference, name of DICOM element to find.
    \param value Variable to hold value found.
    \param id Reference to VolId.
    \return 0 of no match to 'id' is found. Result of dicomFile.Find otherwise.
  */
  template <class T> int
  SeriesHandler::Find(const std::string& s, T& value, const VolId& id) const
  {
    std::string filename;
    if (!GetFirstFilename(id, filename)) { return 0; }
    DicomFile dicomFile(filename.c_str());
    return dicomFile.Find(s, value);
  }

  
  ///
  /** Called as part of conversion process.
  \param v A std::map of VolId to Volume
  */
  template <class T> void
  SeriesHandler::mGetVolumes(std::map<VolId, Volume<T> >& v)
  { 
    v.clear();
    std::string uid = GetSeriesUid();
    wxString message = _("Reading series ") + wxString(uid.c_str(), wxConvLocal);
    if (verbose) { wxLogStatus(message); }

    int image_columns, image_rows;
    //Find(DT_IMAGECOLUMNS, image_columns);
    Find("Columns", image_columns);
    //Find(DT_IMAGEROWS, image_rows);
    Find("Rows", image_rows);

    int file_number = 0;
    FileMapType::const_iterator it = mFileMap.begin();
    FileMapType::const_iterator end = mFileMap.end();

    std::map<VolId, double> minDistMap;
    std::map<VolId, std::string> ippMap;

    int samples_per_pixel;
    Find("SamplesPerPixel", samples_per_pixel);

    // For each file...
    for (;it != end; ++it) {
      std::vector<T> data;
      DicomFile d_file((*it).first.c_str());

      // For each frame/slice in file...
      VolListType::const_iterator vit = it->second.begin();
      for (unsigned int frame = 0; frame < it->second.size(); ++frame, ++vit) {
        // Problem with non-unique volume identifiers shows up here.
        // No, not really. The problem shows later in the processing cycle. -20131216cdt
        if (v.find(*vit) == v.end()) {
          v[*vit] = Volume<T> (image_rows, image_columns);
        }
        
        std::vector<double> rotation = GetRotationMatrix(*vit);
        
        std::string ipp;
        ipp = GetImagePositionPatient(d_file, frame);
        
        double dist = 0;
        for (int i = 0; i < 3; ++i) {
          dist += rotation[i+6] * stof(ipp, i);
        }
        
        double slope = 1;
        double intercept = 0;
        // If rescale, retrieved slope and intercept will cause PixelData to rescale.
        // Otherwise, slope and intercept values cause PixelData to avoid rescale.
		// MOVED TO CONVERSION STEP, fix still underway
        if (rescale) {
          GetRescaleSlopeAndIntercept(d_file, slope, intercept, frame);
        }

        d_file.PixelData<T>(data, image_rows * image_columns * sizeof(T) * samples_per_pixel,
        /*slope, intercept, */frame);
        v[*vit].AddSlice(dist, data, (*it).first);

        if (minDistMap.find(*vit) == minDistMap.end()) {
          minDistMap[*vit] = dist;
          ippMap[*vit] = ipp;
        }

        if (dist < minDistMap[*vit]) {
          minDistMap[*vit] = dist;
          ippMap[*vit] = ipp;
        }
        wxTheApp->Yield();
      }
    }

    std::map<VolId, std::string>::iterator ippIt = ippMap.begin();
    std::map<VolId, std::string>::iterator ippIt_end = ippMap.end();
    for (;ippIt != ippIt_end; ++ippIt) {
      std::vector<double> ipp;
      for (int i = 0; i < 3; ++i) {
        ipp.push_back(stof(ippIt->second, i));
      }
      position[ippIt->first] = ipp;
    }
  }
}

#endif
