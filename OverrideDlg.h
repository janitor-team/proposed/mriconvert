/// OverrideDlg.h
/**
  */

#ifndef OVERRIDE_DLG_H_
#define OVERRIDE_DLG_H_

#include <wx/wx.h>
#include <wx/dialog.h>

namespace jcs {

  class OverrideDlg : public wxDialog
  {
  public:
    OverrideDlg();

    wxCheckBox* dimCheck;
    wxSpinCtrl* skipSpin;

  };

}

#endif
