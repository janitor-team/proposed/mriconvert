/// McFrame.h
/**
*/

#ifndef MC_FRAME_H
#define MC_FRAME_H

#include <wx/frame.h>


class McPanel;

// Define a new frame type
class McFrame : public wxFrame
{
 public:
  // ctor(s)
  McFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
  
  // dtor
  ~McFrame();
  
  // event handlers
  void OnAddFiles		(wxCommandEvent& event);
  void OnAddDir		(wxCommandEvent& event);
  void OnDicom		(wxCommandEvent& event);
  void OnImage		(wxCommandEvent& event);
  void OnInfo			(wxCommandEvent& event);
  void OnRemove		(wxCommandEvent& event);
  void OnDirectory	(wxCommandEvent& event);
  void OnNew			(wxCommandEvent& event);
  void OnConvert		(wxCommandEvent& event);
  void OnRename		(wxCommandEvent& event);
  void OnOptions		(wxCommandEvent& event);
  void OnOverride		(wxCommandEvent& event);
  void OnAbout		(wxCommandEvent& event);
  void OnHelp			(wxCommandEvent& event);
  void OnQuit			(wxCommandEvent& event);

 
protected:

    DECLARE_EVENT_TABLE()

private:

  McPanel*	mpPanel;

};

#endif
