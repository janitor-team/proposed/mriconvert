/// FslNiftiOutputter.cpp
/**
 */

#include <string>
#include <vector>

#include "SeriesHandler.h"
#include "NiftiOutputterBase.h"
#include "FslNiftiOutputter.h"

using namespace jcs;

///
/**
 */
FslNiftiOutputter::FslNiftiOutputter() :
NiftiOutputterBase(CreateOptions())
{
}


///
/**
 */
Options
FslNiftiOutputter::CreateOptions()
{
  Options options = Get3DOptions();
  options.pathname = "FslNifti";
  return options;
}


///
/**
 */
void
FslNiftiOutputter::UpdateOutputForSeries(SeriesHandler* handler)
{
  Basic3DOutputter::UpdateOutputForSeries(handler);
}


///
/** Perform the conversion on the given series.
 *    \param handler A pointer to a SeriesHandler.
 *    \return Status code.
 */
int
FslNiftiOutputter::ConvertSeries(SeriesHandler* handler)
{
  int bits_allocated, pixel_rep = 0;
  handler->Find("BitsAllocated", bits_allocated);
  handler->Find("PixelRepresentation", pixel_rep);

  switch (bits_allocated + pixel_rep) {

    case 8 : {
      FslNiftiConversion<wxUint8> conversion(this, handler);
      conversion.Convert();
      break;
    }
    case 9 : {
      FslNiftiConversion<wxInt8> conversion(this, handler);
      conversion.Convert();
      break;
    }
    case 16 : {
      FslNiftiConversion<wxUint16> conversion(this, handler);
      conversion.Convert();
      break;
    }
    case 17 : {
      FslNiftiConversion<wxInt16> conversion(this, handler);
      conversion.Convert();
      break;
    }
    case 32 : {
      FslNiftiConversion<wxUint32> conversion(this, handler);
      conversion.Convert();
      break;
    }
    case 33 : {
      FslNiftiConversion<wxInt32> conversion(this, handler);
      conversion.Convert();
      break;
    }
    default : {
      wxLogMessage(_("BitsAllocated and PixelRepresentation values (%d, %d) not supported."), bits_allocated, pixel_rep);
    }
  }
  return 1;
}
