///
/**
    */

#ifndef GE_DTI_RB_HANDLER_H
#define GE_DTI_RB_HANDLER_H

#include "SeriesHandler.h"

namespace jcs {
  ///
  /**
    */
  class GeDtiRbHandler : public SeriesHandler
  {
  public:
    GeDtiRbHandler(const std::string& seriesUid);
    virtual GradientInfo GetGradientInfo();
    virtual bool IsDti() const { return true; }

  protected:
    virtual VolListType ReadVolIds(DicomFile& file);

  };
}

#endif
