/// MetaOptionsDlg.cpp
/**
*/

#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>

#include "MetaOptionsDlg.h"
#include "NewMetaOutputter.h"

using namespace jcs;

BEGIN_EVENT_TABLE(MetaOptionsDlg, wxDialog)

EVT_BUTTON(wxID_OK, MetaOptionsDlg::OnOkay)
EVT_BUTTON(wxID_CLEAR, MetaOptionsDlg::OnClear)

END_EVENT_TABLE()

///
/**
*/
MetaOptionsDlg::MetaOptionsDlg(NewMetaOutputter* outputter)
: BasicOptionsDlg(_("MetaImage options"), outputter),
  mOutputter(outputter)
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dlgSizer->Add(myPanel);

  wxString check_text(_("Save header only "));
  mpHeaderOnlyCheck = new wxCheckBox(this, -1, check_text);
  mpHeaderOnlyCheck->SetValue(mOutputter->SaveHeaderOnly());
  dlgSizer->Add(mpHeaderOnlyCheck, 0, wxALL, 10);

  dlgSizer->Add(new wxStaticText(this, -1, _("Add fields")), 0, wxTOP|wxLEFT, 10);
  mpCheckList = new wxCheckListBox(this, -1);

  Dictionary* dicom = Dicom_Dictionary::Instance();
  std::vector<std::string> tagList = dicom->TagList();
  for (unsigned int i = 0; i < tagList.size(); ++i) {
    mAddTag(tagList.at(i));
  }

  mpCheckList->SetSize(mpCheckList->GetBestSize());
  dlgSizer->Add(mpCheckList, 1, wxEXPAND|wxALL, 10);

  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  buttonSizer->Add(new wxButton(this, wxID_CLEAR, _("Clear fields")), 0, wxRIGHT, 10);
  wxButton* okayButton = new wxButton(this, wxID_OK, _("Okay"));
  buttonSizer->Add(okayButton, 0, wxRIGHT, 10);
  buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));
  okayButton->SetDefault();

  dlgSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);

  mNeedsRebuild = false;
}


///
/**
*/
void
MetaOptionsDlg::SetFields(std::vector<std::string> fields)
{
  for (unsigned int i = 0; i < fields.size(); ++i) {
    mCheckItem(fields.at(i));
  }
}


///
/**
*/
std::vector<std::string> 
MetaOptionsDlg::GetFields() const
{
  std::vector<std::string> fields;
  int n_items = mpCheckList->GetCount();
  for (int i = 0; i < n_items; ++i) {
    if (mpCheckList->IsChecked(i)) {
      fields.push_back(static_cast<const char*>(mpCheckList->GetString(i).mb_str(wxConvLocal)));
    }
  }
  return fields;
}


///
/**
*/
int
MetaOptionsDlg::mAddTag(const std::string tag)
{
  if (tag.compare(0, 2, "Mf")) { // ignore metafile elements
    mpCheckList->Append(wxString(tag.c_str(), wxConvLocal));
  }
  return 1;
}


///
/**
*/
int
MetaOptionsDlg::mCheckItem(const std::string item)
{
  int index = mpCheckList->FindString(wxString(item.c_str(), wxConvLocal));
  mpCheckList->Check(index);
  return 1;
}


///
/**
*/
void
MetaOptionsDlg::OnClear(wxCommandEvent& event)
{
  int n_items = mpCheckList->GetCount();
  for (int i = 0; i < n_items; ++i)
    mpCheckList->Check(i, false);
}


///
/**
*/
void
MetaOptionsDlg::OnOkay(wxCommandEvent& event)
{
  bool rbld = Rebuild();
  bool nfSave = SaveNameFields();
  bool hdrOnly = (SaveHeaderOnly() != mOutputter->SaveHeaderOnly());
  mNeedsRebuild = rbld || hdrOnly || nfSave;
  BasicOptionsDlg::OnOkay(event);
  mOutputter->SetSaveHeader(SaveHeaderOnly());
  mOutputter->fields = GetFields();

  EndModal(event.GetId());
}
