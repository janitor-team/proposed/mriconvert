/// DicomTree.h
/** Defines DicomTreeCtrl, the control that holds the list of inputs to process.
*/

#ifndef DICOM_TREE_H
#define DICOM_TREE_H

#include "jcsTree.h"

namespace jcs { 
  
	class SeriesHandler;

	class DicomTreeCtrl : public jcsTreeCtrl
	{	
	public:

		DicomTreeCtrl(wxWindow* parent, wxWindowID id, long more_styles);
		DicomTreeCtrl(wxWindow* parent, wxWindowID id);
		void AddSeries(SeriesHandler* series);
		std::vector<std::string> GetChildSeries(wxTreeItemId branch);
		std::vector<std::string> GetFilenames();
		std::vector<wxString> GetFilesInSeries(const wxTreeItemId item);
	};

}
#endif
