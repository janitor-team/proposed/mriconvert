/// InfoFrame.cpp
/**
 */

#if defined(__WXGTK__) || defined(__WXMOTIF__)
  #include <wx/wx.h>
#endif

#include <wx/wxprec.h>
#include <wx/frame.h>

#include <vector>
#include <string>
#include <algorithm>
#include <functional>

#include "InfoFrame.h"


///
/**
*/
InfoFrame::InfoFrame(wxWindow* parent, std::vector<std::string> info)
: wxFrame(parent, -1, _T("Series info"))
{
  wxBoxSizer* frameSizer = new wxBoxSizer(wxVERTICAL);

  mTextCtrl = new wxTextCtrl(this, -1, _T(""),
  wxDefaultPosition, wxSize(400,400),
  wxTE_MULTILINE | wxTE_READONLY | wxTE_RICH);

  frameSizer->Add(mTextCtrl, 1, wxEXPAND);

  std::vector<std::string>::iterator it = info.begin();
  std::vector<std::string>::iterator it_end = info.end();
  for (; it != it_end; ++it) {
    AddString(*it);
  }

  SetSizer(frameSizer);

  frameSizer->Fit(this);
  mTextCtrl->SetInsertionPoint(0);

}


///
/**
*/
int
InfoFrame::AddString(std::string& s)
{
  mTextCtrl->AppendText(wxString(s.c_str(), wxConvLocal));
  mTextCtrl->AppendText(_T("\n"));

  return 1;
}
