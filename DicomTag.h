/// DicomTag.h
/** Structure represents a DICOM tag as a group and element tuple.
    Can initialize by ctor or from an input stream.
    Suggested Unit Tests
    - initialize by ctor, test boolean operators
    - initialize by input stream, test boolean operators
    - initialize by ctor, re-initialize by input stream, test boolean operators
    - test assignment of group and element, should fail, these should be 
      set only via ctor or input stream; should not even compile
*/

#ifndef DICOMTAG_H_
#define DICOMTAG_H_

#include <map>
#include <vector>
#include <string>
#include <istream>
#include <iostream>

#include <wx/defs.h>

enum {
LEI_CODE,
LEE_CODE,
BEE_CODE
};

namespace jcs {
  struct DicomTag {
    public:
    DicomTag() {}
    DicomTag(wxUint16 g, wxUint16 e) : group(g), element(e) {}

    wxUint16 group;
    wxUint16 element;
    wxUint8 transferSyntaxCode;
    private:
    
    public:
    bool operator== (const DicomTag &rhs) const;
    bool operator!= (const DicomTag &rhs) const;
    bool operator<  (const DicomTag &rhs) const;
    bool operator>  (const DicomTag &rhs) const;
    friend std::istream &operator>>(std::istream &in, DicomTag &dt);
  };
  std::istream &operator>>(std::istream &in, DicomTag &dt);
  std::ostream &operator<<(std::ostream &out, const DicomTag &rhs);
}

#endif
