/// SpmOptionsDlg.cpp
/**
*/

#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>

#include "SpmOptionsDlg.h"
#include "NewSpmOutputter.h"

using namespace jcs;

BEGIN_EVENT_TABLE(SpmOptionsDlg, BasicOptionsDlg)

EVT_BUTTON(wxID_OK, SpmOptionsDlg::OnOkay)

END_EVENT_TABLE()


SpmOptionsDlg::SpmOptionsDlg(NewSpmOutputter* outputter)
: BasicOptionsDlg(_("SPM Analyze options"), outputter), 
  mOutputter(outputter)
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dlgSizer->Add(myPanel);

  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  wxButton* okayButton = new wxButton(this, wxID_OK, _("Okay"));
  buttonSizer->Add(okayButton, 0, wxRIGHT, 10);
  buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));
  okayButton->SetDefault();

  dlgSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);

  mNeedsRebuild = false;

}

void
SpmOptionsDlg::OnOkay(wxCommandEvent& event)
{
  bool rbld = Rebuild();
  bool nfSave = SaveNameFields();
  mNeedsRebuild = rbld || nfSave;
  BasicOptionsDlg::OnOkay(event);
  EndModal(event.GetId());
}
