/// OutputTreeNew.h
/** Classes used to display the folder heirarchy and list
    of files to be produced.
*/

#ifndef OUTPUT_TREE_NEW_H
#define OUTPUT_TREE_NEW_H

#include <wx/treectrl.h>

#include <string>
#include <map>
#include <vector>

#include "OutputList.h"

class McPanel;

namespace jcs {

	class NewOutputTreeCtrl;
	class OutputTreeState;
	class OutputterBase;

	class OutputTreeItemData : public wxTreeItemData
	{
	public:

		OutputTreeItemData(const std::string& uid, NewOutputTreeCtrl& treeCtrl) 
		: mUid(uid), mTreeCtrl(treeCtrl) {}

		virtual void FillContextMenu(wxMenu& menu);
		virtual void OnRename() = 0;
		virtual bool IsEditable() = 0;

		const std::string& GetUid() const { return mUid; }

		virtual std::string GetSeriesUid() const { return ""; }

	protected:
		NewOutputTreeCtrl& mTreeCtrl;

	private:

		const std::string mUid;

	};

	// only 3 item types: root, file, dir

	class DirItem : public OutputTreeItemData
	{
	public:

		DirItem(const std::string& uid, NewOutputTreeCtrl& treeCtrl) 
		: OutputTreeItemData(uid, treeCtrl) {}

		virtual bool IsEditable() { return true; }
		virtual void OnRename();
	};


	class FileItem : public OutputTreeItemData
	{
	public:
		FileItem(const std::string& uid, NewOutputTreeCtrl& treeCtrl) 
		: OutputTreeItemData(uid, treeCtrl) {}

		virtual bool IsEditable() { return false; }

		virtual void OnRename();

		std::string GetSeriesUid() const { return mSeriesUid; }
		void SetSeriesUid(const std::string& uid) { mSeriesUid = uid; }
		const std::string& GetPrefix() const { return mPrefix; }
		void SetPrefix(const std::string& prefix) { mPrefix = prefix; }

		virtual void FillContextMenu(wxMenu& menu);

	private:
		std::string mSeriesUid; 
		std::string mPrefix;    
	};

	class RootItem : public OutputTreeItemData
	{
	public:
		RootItem(const char* uid, NewOutputTreeCtrl& treeCtrl) 
		: OutputTreeItemData(uid, treeCtrl) {}

		virtual bool IsEditable() { return false; }
		virtual void OnRename();
	};

	class NewOutputTreeCtrl : public wxTreeCtrl
	{
	public:

		NewOutputTreeCtrl(McPanel* parent, wxWindowID id, const wxString& root);
		~NewOutputTreeCtrl();

		wxTreeItemId FindTreeItem(wxTreeItemId parent, const std::string& uid);
		void Reset();
		void RemoveBranch(wxTreeItemId branch);

		void RenameFiles(std::string series_uid, std::string prefix);
		void RenameDir(wxTreeItemId diritem);

		std::map<std::string, std::string> GetOutDirs();

		void SplitDirectories(bool split = true) {}
		void SplitSubjects(bool split = true) {}

		void OnContextMenu(wxTreeEvent& event);

		void OnContextMenuTest(wxContextMenuEvent& event);
		void OnMenuOverride(wxCommandEvent& event);
		void OnMenuOptions(wxCommandEvent& event);
		void OnMenuRename(wxCommandEvent& event);


		void OnEditBegin(wxTreeEvent& event);

		void SetOutputter(OutputterBase* outputter);

		void RefreshList();
		void RemoveSeries(const std::string& seriesUid);

		void ChooseRootDir();
		void SetRoot(const wxString& text);

		enum {
			folder_icon,
			open_folder_icon,
			file_icon
		};

		// temporary?
		int GetItemType(const wxTreeItemId& item) const;
		int GetSelectionType() const;
		wxString GetStringSelection() const; 
		std::string GetPrefix(const wxTreeItemId& item) const { return ""; }

		OutputterBase* mOutputter;

	protected:

		DECLARE_EVENT_TABLE()

	private:

		bool HasVisibleRoot();
		void CreateImageList(int size = 16);

		void CheckChildren(wxTreeItemId& item, std::vector<wxTreeItemId>& items, const std::string& seriesUid);

		McPanel* parentPanel;
	};

} // end namespace jcs

#endif
