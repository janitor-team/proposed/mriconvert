/// BasicOptionsDlg.h
/**
*/

#ifndef BASIC_OPTIONS_DLG_H_
#define BASIC_OPTIONS_DLG_H_

#include <vector>
#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/panel.h>

namespace jcs {

  class OutputterBase;
  class Basic3DOutputter;

  /// Base class for most options dialogs.
  class BasicOptionsDlg : public wxDialog
  {
  public:
    BasicOptionsDlg(const wxString& title, Basic3DOutputter* outputter);
    bool SplitDirs() const;
    bool SplitSubj() const;
    bool Rescale() const;
    bool Rebuild() const;
    int Dimensionality() const;
    int Skip() const;
    void OnOkay(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);
    std::vector<wxString> NameFields() const;

  protected:
    DECLARE_EVENT_TABLE()
    void CmdEvtHandler(wxCommandEvent &evt);
    void OnRightClick(wxMouseEvent &event);
    class BasicPanel : public wxPanel
    {
    public:
      BasicPanel(wxWindow* parent);

      /// If true, will create a sub-directory for each series & subject
      wxCheckBox* mpSplitDirsCheck;

      // If true, will create a sub-drectory for each subject
      wxCheckBox* mpSplitSubjCheck;

      /// If true, dimensionality is 4, else 3.
      wxCheckBox* mpDimCheck;

      /// Number of volumes to skip for multi-volume series.
      wxSpinCtrl* mpSkipSpin;

      /// DICOM field names to use in output file names.
      wxCheckListBox* mpNameFields;

      /// If true, apply rescale slope and intercept processing.
      wxCheckBox* mpRescaleCheck;
    };    

    bool SaveNameFields();

    BasicPanel* myPanel;

  private:
    Basic3DOutputter* mOutputter;

  };

}

#endif
