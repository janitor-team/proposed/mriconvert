/// McPanel.h
/** Main user interface within McFrame.
*/

#ifndef MC_PANEL_H
#define MC_PANEL_H

#include <wx/wx.h>
#include <wx/dnd.h>

#include <utility>
#include <string>

#include "OutputTreeNew.h"
#include "OutputterBase.h"

namespace jcs {
  class Converter;
  class DicomTreeCtrl;
  class OutputTreeCtrl;
}

class wxTreeEvent;
class wxSpinCtrl;

wxDECLARE_EVENT(DROP_EVENT, wxCommandEvent);

/// Tooltip button
/** Convenience class, creates a button with tool tip.
*/
class ttButton : public wxButton
{
public:
  ttButton(wxWindow* parent, wxWindowID window,
  const wxString& ButtonText, const wxString& HelpText)
  : wxButton(parent, window, ButtonText)
  { SetToolTip(HelpText); }

};

// Define a new frame type
class McPanel : public wxPanel
{
public:
  McPanel(wxWindow* parent);
  ~McPanel();

  // event handlers
  void OnAddDir(wxCommandEvent& event);
  void OnAddFiles(wxCommandEvent& event);
  void OnRemove(wxCommandEvent& event);
  void OnInfo(wxCommandEvent& event);
  void OnDirectory(wxCommandEvent& event);
  void OnNew (wxCommandEvent& event);
  void OnConvert (wxCommandEvent& event);
  void OnRename(wxCommandEvent& event);
  void OnDicom(wxCommandEvent& event);
  void OnImage(wxCommandEvent& event);
  void OnOptions (wxCommandEvent& event);
  void OnQuit(wxCommandEvent& event);
  void OnChoiceOutput(wxCommandEvent& event);
  void OnContextMenu(wxContextMenuEvent& event);
  //void OnDropFiles(wxDropFilesEvent& event);
  void OnSourceSelect(wxTreeEvent& event);
  void OnOutputSelect(wxTreeEvent& event);
//  void OnEditEnd (wxTreeEvent& event);
  void OnEditBegin(wxTreeEvent& event);
  void OnOverride(const std::string& seriesUid);
  void OnMenuOverride();

 void OnDropFiles(const wxArrayString& dropfiles);

  enum {
    COMMAND_REMOVE,
    COMMAND_DICOM,
    COMMAND_INFO,
    COMMAND_IMAGE,
    COMMAND_ADD_FILES,
    COMMAND_ADD_DIR,
    COMMAND_OPTIONS,
    COMMAND_DIRECTORY,
    COMMAND_NEW,
    COMMAND_CONVERT,
    COMMAND_RENAME,
    CHOICE_OUTPUT,
    SOURCE_CTRL,
    OUTPUT_CTRL
  };


protected:

  DECLARE_EVENT_TABLE()

private:

//  void SetPathInfo(wxString path);
  jcs::DicomTreeCtrl* mpSourceCtrl;
  jcs::NewOutputTreeCtrl* mpOutputCtrl;

  ttButton* mpRemoveButton;
  ttButton* mpRenameButton;
  ttButton* mpOptionsButton;

  wxChoice* mpOutputChooser;
  int m_output_type;

//  wxStaticText* mpPathInfo;
  
  jcs::Converter* mpConverter;
  void SetSplitDirs(bool split);
  void SetSplitSubj(bool split);

  void RefreshSourceCtrl();
  void ResetOutput();

  void UpdateOutputCtrl();
  void ShowMessageBox();
  void mAddSomeFilesDialog(wxArrayString files, const wxChar* msg = _("Adding files..."));

  friend class jcs::NewOutputTreeCtrl;
  bool ShowOptionsDlg(int type, jcs::OutputterBase* outputter);
  bool ShowOverride(int type, jcs::OutputterBase* outputter, const std::string& series_uid);
};

class DnDFile : public wxFileDropTarget
{
  public:
    DnDFile(McPanel *pOwner = NULL) { m_pOwner = pOwner; }

    virtual bool OnDropFiles(wxCoord x, wxCoord y,
                             const wxArrayString& filenames);

  private:
    McPanel *m_pOwner;

};

#endif
