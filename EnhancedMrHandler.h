/// EnhancedMrHandler.h
/**
*/

#ifndef ENHANCED_MR_HANDLER_H
#define ENHANCED_MR_HANDLER_H

#include "SeriesHandler.h"

namespace jcs {
  ///
  /**
    */
  class EnhancedMrHandler : public SeriesHandler
  {
  public:
    EnhancedMrHandler(const std::string& seriesUid);
    virtual GradientInfo GetGradientInfo();
    virtual bool IsDti() const;
    virtual std::vector<std::string> GetStringInfo();
    virtual std::vector<double> GetVoxelSize();
    virtual int GetRescaleSlopeAndIntercept(DicomFile& dfile, double& slope, double& intercept, int frame = 0) const;

  protected:
    virtual VolListType ReadVolIds(DicomFile& file);
    virtual std::string GetImagePositionPatient(DicomFile& dfile, int frame);

  private:
    std::vector<std::string> ipps;
    std::vector<double> slopes;
    std::vector<double> intercepts;
    std::map<VolId, std::string> bvecMap;
    std::map<VolId, std::string> bvalMap;
    std::map<VolId, int> volFrameMap;
    std::string pixelSpacing;

  };
};

#endif
