/// NiftiOutputterBase.cpp
/**
*/

#include "NiftiOutputterBase.h"
#include "NiftiVolume.h"

using namespace jcs;

NiftiOutputterBase::NiftiOutputterBase(const Options& options) : 
Basic3DOutputter(options)
{
  saveNii = (rawExtension == _T("")) ? true : false;
}


///
/*
*/
BasicVolumeFormat* 
NiftiOutputterBase::GetOutputVolume(const char* file)
{
  return new NiftiVolume(file, headerExtension.mb_str(wxConvLocal), rawExtension.mb_str(wxConvLocal));
}


///
/**
*/
void
NiftiOutputterBase::SetSaveNii(bool value)
{
  saveNii = value;
  if (saveNii) {
    headerExtension = _T("nii");
    rawExtension = _T("");
  }
  else {
    headerExtension = _T("hdr");
    rawExtension = _T("img");
  }
}


/// Sets a boolean option.
/** Calls parent SetOption and sets "nii" option.
    \param name The name of the option.
    \param value The new value of the option.
*/
void
NiftiOutputterBase::SetOption(const std::string& name, bool value)
{
  Basic3DOutputter::SetOption(name, value);
  if (name.find("nii") != std::string::npos) {
    SetSaveNii(value);
  }
}
