#ifndef TEXT_FILE_VIEWER_H
#define TEXT_FILE_VIEWER_H

#include <wx/frame.h>

class wxTextCtrl;

//****
// This used to read and display text files, now it just
// displays the contents provided by certain .h files.
//****
class TextFileViewer: public wxFrame
{
  public:
    TextFileViewer(wxWindow* parent, const wxChar* caption);

  private:
    wxTextCtrl* mTextCtrl;
};

#endif
