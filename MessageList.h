#ifndef MESSAGE_LIST_H_
#define MESSAGE_LIST_H_

#include <vector>
#include <wx/fdrepdlg.h>


class wxListView;


namespace jcs {

typedef std::vector<wxString> Message;
typedef std::vector<Message> MessageList;


class MessageListDlg : public wxDialog
{
public:
	MessageListDlg(const wxString& title);
	void AddMessages(const MessageList& messages);
	void SetColumns(const Message& columns);

	void OnSave (wxCommandEvent& event);

protected:

    DECLARE_EVENT_TABLE()

private:
	wxListView* mpListView;
	wxString mGetItemText(long index, int column) const;

};

}

#endif

