/// OutputFactory.cpp
/**
 */

#include "OutputFactory.h"

using namespace jcs;

OutputterBase*
OutputFactory::CreateNewOutputter(int type)
{
	OutputterBase* pOutputter;

	switch (type) {

		case SPM :
			pOutputter = new NewSpmOutputter();
			break;

		case META :
			pOutputter = new NewMetaOutputter();
			break;

		case NIFTI :
			pOutputter = new NiftiOutputter();
			break;

		case ANALYZE :
			pOutputter = new AnalyzeOutputter();
			break;

		case FSL :
			pOutputter = new FslNiftiOutputter();
			break;

		case BV :
			pOutputter = new NewBvOutputter();
			break;

		default :
			pOutputter = new NewSpmOutputter();
		}

	return pOutputter;
}


const char*
OutputFactory::GetDescription(int type)
{
	switch (type) {
		case SPM : return "SPM Analyze";
		case META: return "Meta Image";
		case NIFTI: return "NIfTI";
		case ANALYZE: return "Analyze 7.5";
		case FSL: return "FSL NIfTI";
		case BV: return "BrainVoyager";
		default : return "Unknown type";
	}
}

const char*
OutputFactory::GetShortDescription(int type)
{
	switch (type) {
		case SPM : return "spm";
		case META: return "meta";
		case NIFTI: return "nifti";
		case ANALYZE: return "analyze";
		case FSL: return "fsl";
		case BV: return "bv";
		default : return "Unknown type";
	}
}
