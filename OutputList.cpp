/// OutputList.cpp
/**
*/

#include <wx/file.h>
#include <wx/filename.h>
#include <string>
#include <algorithm>
#include <set>
#include <iostream>

#include "OutputList.h"

using namespace jcs;


///
/**
*/
bool
IsInSeries(const OutputList::ListType::value_type list_item, const std::string& seriesUid)
{
  return list_item.second.seriesUid == seriesUid;
}


///
/**
*/
ImageFileName::ImageFileName()
{
  isRenameable = true;
}


///
/**
*/
void
ImageFileName::SetPrefix(const std::string& prefix)
{
  mPrefix = prefix.c_str();
}


///
/**
*/
void
ImageFileName::SetPostfix(const std::string& postfix)
{
  mPostfix = postfix.c_str();
}


///
/**
*/
std::string
ImageFileName::GetPrefix() const
{
  return mPrefix;
}


///
/**
*/
std::string
ImageFileName::GetPostfix() const
{
  return mPostfix;
}


///
/**
*/
void
ImageFileName::AppendDir(const std::string& uid, const std::string& dir)
{
  DirPair dirPair;
  dirPair.uid = uid;
  dirPair.dir = dir;
  mDirList.push_back(dirPair);
}


///
/**
*/
int
ImageFileName::GetDirCount() const
{
  return mDirList.size();
}


///
/**
*/
void
ImageFileName::ResetPath()
{
  mDirList.clear();
}


///
/**
*/
std::string
ImageFileName::GetPath() const
{
  std::string path;
  DirListType::const_iterator it = mDirList.begin();
  DirListType::const_iterator it_end = mDirList.end();
  for (;it != it_end; ++it) {
    path += it->dir;
    path += "/";  //GetPathSeparator
  }
  return path;
}


///
/**
*/
std::string
ImageFileName::GetFullName() const
{
  std::string filename = mPrefix + mPostfix;
  if (!mExt.empty()) {
    filename += "." + mExt;
  }
  return filename;
}


///
/**
*/
std::string
ImageFileName::GetFullPath() const
{
  return GetPath() + GetFullName();
}


///
/**
*/
std::string
ImageFileName::GetExt() const
{
  return mExt;
}


///
/**
*/
void
ImageFileName::SetExt(const std::string& ext)
{
  mExt = ext;
}


///
/**
*/
std::string
ImageFileName::GetDirName(int pos) const
{
  return mDirList.at(pos).dir;
}


///
/**
*/
std::string
ImageFileName::GetDirUid(int pos) const
{
  return mDirList.at(pos).uid;
}


///
/**
*/
void
ImageFileName::SetDirName(int pos, std::string name)
{
  mDirList.at(pos).dir = name;
}


///
/**
*/
wxFileName
OutputList::GetFullFileName(const std::string& key) const
{
  wxFileName name;

  ListType::const_iterator pos = fileNameList.find(key);

  if (pos != fileNameList.end()) {
    name = wxFileName(rootDir + wxFileName::GetPathSeparator() + pos->second.GetFullPath());
  }
  else {
    name.SetName(_T("error"));
  }

  return name;
}
