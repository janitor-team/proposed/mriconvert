/// StringConvert.h
/**
*/

#ifndef STRINGCONVERT_H_
#define STRINGCONVERT_H_

#include <algorithm>
#include <functional>
#include <string>
#include <vector>
#include <sstream>

namespace jcs {

  // no, it doesn't make sense to put this here, but that's where it is for now.
  //extern bool verbose;

  float stof(const std::string& rString);
  float stof(const std::string& rString, int n);
  int stoi(const std::string& rString);
  int stoi(const std::string& rString, int n);
  std::string itos(const int i, int width = 0);
  std::string ftos(const double f);
  std::string ftos(const double f, int precision);
  std::string Date(const std::string& date);

  std::vector<std::string> ParseDicomString(const std::string& str);

  template <class T>
  struct member_of : std::binary_function 
  <T, std::basic_string<T>, bool> 
  {
    bool operator()(const T& c, const std::basic_string<T>& s) const
    { return (s.find(c) != std::basic_string<T>::npos); }
  };


  std::string RemoveInvalidChars(const std::string& name);

}

#endif
