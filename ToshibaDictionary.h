///
/**

    As defined in Toshiba conformance Statement MIIMR0005EA (2006).
    Excelart series. Note that MIIM0006EAB (2011, 0xVantage-Titan) 
    specifies no private tags.
*/
//group, 0xelement name VR VM
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0x00xx ), "Private Creator Code", "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx00 ), "Scale Factor", "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx01 ), "Acquisition Order", "OB" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx02 ), "Orientation Vector", "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx03 ), "Flip Flag", "SS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx04 ), "Rotate Information", "OB" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx05 ), "FOV", "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx06 ), "Image Matrix", "US" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx07 ), "Image Information", "OB" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx08 ), "Original Data", "OB" ));
mAddEntry ( DicomElement ( DicomTag ( 0x700D, 0xxx09 ), "Original Data Flag", "SS" ));
