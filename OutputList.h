/// OutputList.h
/**
*/

#ifndef OUTPUT_LIST_H_
#define OUTPUT_LIST_H_

#include <string>
#include <vector>
#include <map>
#include <wx/string.h>
#include <wx/filename.h>


namespace jcs {

  class ImageFileName {

  public:
    ImageFileName();

    void SetPrefix(const std::string& prefix);
    void SetPostfix(const std::string& postfix);
    std::string GetPrefix() const;
    std::string GetPostfix() const;
    void SetExt(const std::string& ext);

    void AppendDir(const std::string& uid, const std::string& dir);
    int GetDirCount() const;
    void ResetPath();

    std::string GetFullName() const;
    std::string GetFullPath() const;
    std::string GetPath() const;
    std::string GetExt() const;
    std::string GetDirName(int pos) const;
    std::string GetDirUid(int pos) const;
    
    void SetDirName(int pos, std::string name);

    bool isRenameable;

    std::string seriesUid;

  private:
    std::string mPrefix;
    std::string mPostfix;
    std::string mExt;

    struct DirPair {
      std::string uid;
      std::string dir;
    };
    typedef std::vector<DirPair> DirListType;
    DirListType mDirList;
  };


  struct OutputList {

    // maps a list of unique file identifiers to a list of output files -- each id = 1 volume, but 
    // could be multiple files out (ie, .hdr + .img)
    typedef std::multimap<std::string, ImageFileName> ListType;

    ListType fileNameList;
    wxString rootDir;

    wxFileName GetFullFileName(const std::string& key) const;

  };
}

#endif
