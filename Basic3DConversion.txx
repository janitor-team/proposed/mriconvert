/// Basic3DConversion.txx
/**
*/

#ifndef BASIC_3D_CONVERSION_TXX_
#define BASIC_3D_CONVERSION_TXX_

#include <string>
#include <map>

#include <wx/log.h>

#include "Volume.h"
#include "OutputterBase.h"
#include "BasicVolumeFormat.h"

namespace jcs {

  ///
  /**
    */
  template <class T>
  Basic3DConversion<T>::Basic3DConversion(Basic3DOutputter* outputter, SeriesHandler* handler)
  : mOutputter(outputter), mHandler(handler)
  {
  }


  ///
  /** Gets the value of mOutputter's skip setting
      for the series associated with mHandler.
      \return Number of volumes to skip. If the skip setting
              exceeds the number of volumes, returns 0.
    */
  template <class T> int
  Basic3DConversion<T>::GetSkip()
  {
    int retval = 0;
    std::string series_uid = mHandler->GetSeriesUid();
    int nVolumes = mHandler->GetNumberOfVolumes();
    int skip = mOutputter->GetSkip(series_uid);
    if (nVolumes > skip) {
      retval = skip;
    }
    return retval;
  }


  ///
  /** Gets the number of volumes to be converted, that is
      the number of volumes associated with mHandler less
      the skip setting.
      \return Number of volumes to be converted.
    */
  template <class T> int
  Basic3DConversion<T>::GetNumberOfVolumes()
  {
    int nVolumes = mHandler->GetNumberOfVolumes();
    return nVolumes - GetSkip();
  }


  /// The main reason for being.
  /** This is the heart of MRIConvert.
    */
  template <class T> void
  Basic3DConversion<T>::Convert()
  {
    vMapType volumes;
    mHandler->rescale = mOutputter->rescale;
    mHandler->GetVolumes(volumes);

    GetHeaderForSeries();

    std::string series_uid = mHandler->GetSeriesUid();
    int dimensionality = mOutputter->GetDimensionality(series_uid);

    if (dimensionality == 4) {
      
      wxFileName file = mOutputter->GetFileName(series_uid);
      if (file.GetName() == _T("error")) {
        wxLogError(_T("File name error"));
        return;
      }
      file.SetExt(_T(""));

      BasicVolumeFormat* outputVolume = mOutputter->GetOutputVolume(file.GetFullPath());

      typename vMapType::iterator it = volumes.begin();
      typename vMapType::iterator it_end = volumes.end();
      CompleteHeaderForVolume(*it);

      outputVolume->WriteHeader(GetHeader());

      for (int n = 0; n < GetSkip(); ++n, ++it);
      for (;it != it_end; ++it) {
        ProcessSlicesAndAppend(outputVolume, it);
        wxTheApp->Yield();
      }
      delete outputVolume;

    } else { // dimensionality presumably == 3

      typename vMapType::iterator it = volumes.begin();
      typename vMapType::iterator it_end = volumes.end();

      for (int n = 0; n < GetSkip(); ++n, ++it);

      for (;it != it_end; ++it) {
        CompleteHeaderForVolume(*it);

        wxFileName file = mOutputter->GetFileNameFromVolId(it->first);
        if (file.GetName() == _T("error")) {
          wxLogError(_T("File name error"));
          break;
        }

        file.SetExt(_T(""));
        BasicVolumeFormat* outputVolume = mOutputter->GetOutputVolume(file.GetFullPath());
        outputVolume->WriteHeader(GetHeader());
        ProcessSlicesAndAppend(outputVolume, it);
        delete outputVolume;
        wxTheApp->Yield();
      }
    }

    // Write additional informational text files.
    if (this->mHandler->IsMoCo()) {
      WriteMoCoFiles();
    }
    WriteStringInfo();
  }


  ///
  /**
      \param outputVolume Pointer to BasicVolumeFormat instance.
      \param it Iterator through vMapType structure.
    */
  template <class T> void
  Basic3DConversion<T>::ProcessSlicesAndAppend(BasicVolumeFormat* outputVolume, typename vMapType::iterator it)
  {
    typedef std::map<double, std::vector<T> > SliceMap;
    typename SliceMap::iterator slice_it = it->second.begin();
    typename SliceMap::iterator slice_end = it->second.end();
    for (; slice_it != slice_end; ++slice_it) {
      ProcessSlice(slice_it->second);
	  double slope, intercept;
	  if (this->mHandler->rescale &&
		  this->mHandler->GetRescaleSlopeAndIntercept(it->first, slope, intercept)) {
			  std::vector<float> float_slice;
              // Use typename for GCC C++11 compliance. 20160120cdt
			  for (typename std::vector<T>::iterator dit = slice_it->second.begin(); dit != slice_it->second.end(); ++dit) {
					float_slice.push_back(static_cast<float>(*dit * slope + intercept));
			  }
			outputVolume->AppendRawData(reinterpret_cast<char*> (&float_slice.front()), float_slice.size() * sizeof(float_slice.front()));
	  }
	  else {
		outputVolume->AppendRawData(reinterpret_cast<char*> (&slice_it->second.front()),
			slice_it->second.size() * sizeof(slice_it->second.front()));
	  }
    }
  }


  ///
  /** Writes MoCo text files.
    */
  template <class T> void
  Basic3DConversion<T>::WriteMoCoFiles()
  {
    std::vector<std::string> moco;
    this->mHandler->GetImageComments(moco);

    if (moco.size() != 0) {
      std::string key = this->mHandler->GetSeriesUid() + this->mOutputter->moco_postfix;
      wxFileName fileName = this->mOutputter->mOutputList.GetFullFileName(key);
      IterativeWrite(fileName, moco);
    }
  }


  ///
  /** Writes DICOM information file for a series.
  */
  template <class T> void
  Basic3DConversion<T>::WriteStringInfo()
  
  {
    std::string key = this->mHandler->GetSeriesUid() + "_info";
    wxFileName fileName = this->mOutputter->mOutputList.GetFullFileName(key);
    std::vector<std::string> writeThis = this->mHandler->GetStringInfo();
    IterativeWrite(fileName, writeThis);
  }


  ///
  /** Writes gradient information files (bvecs, bvals) for a series.
  */
  template <class T> void
  Basic3DConversion<T>::WriteGradientFiles()
  {
    GradientInfo info = this->mHandler->GetGradientInfo();
    Normalize(info);

    if (info.values.size() == 0) {
      return;
    }

    // First write the bvals file.
    std::string key = this->mHandler->GetSeriesUid() + this->mOutputter->bvals_postfix;
    wxFileName fileName = this->mOutputter->mOutputList.GetFullFileName(key);
    std::vector<std::string> writeThis;

    if (fileName.GetName() == _T("error")) {
      wxLogError(_T("File name error"));
      return;
    }
    
    std::vector<double>::iterator it = info.values.begin();
    std::vector<double>::iterator it_end = info.values.end();
    std::stringstream ss;
    for (; it != it_end; ++it) {
      ss << *it << " ";
    }
    writeThis.push_back(ss.str());
    ss.clear();
    ss.str(std::string());
    IterativeWrite(fileName, writeThis);
    writeThis.clear();

    // Now write bvecs file.
    key = this->mHandler->GetSeriesUid() + this->mOutputter->bvecs_postfix;
    fileName = this->mOutputter->mOutputList.GetFullFileName(key);

    if (fileName.GetName() == _T("error")) {
      wxLogError(_T("File name error"));
      return;
    }

    std::vector<double>::iterator x_it = info.xGrads.begin();
    std::vector<double>::iterator x_it_end = info.xGrads.end();
    for (; x_it != x_it_end; ++x_it) {
      ss << *x_it << " ";
    }
    writeThis.push_back(ss.str());
    ss.clear();
    ss.str(std::string());

    std::vector<double>::iterator y_it = info.yGrads.begin();
    std::vector<double>::iterator y_it_end = info.yGrads.end();
    for (; y_it != y_it_end; ++y_it) {
      if (*y_it == 0) {
        ss << (*y_it) << " ";
      }
      else {
        ss << -(*y_it) << " ";
      }
    }
    writeThis.push_back(ss.str());
    ss.clear();
    ss.str(std::string());

    std::vector<double>::iterator z_it = info.zGrads.begin();
    std::vector<double>::iterator z_it_end = info.zGrads.end();
    for (; z_it != z_it_end; ++z_it) {
      ss << *z_it << " ";
    }
    writeThis.push_back(ss.str());
    ss.clear();
    ss.str(std::string());

    IterativeWrite(fileName, writeThis);
  }


  ///
  /** Writes each string in the given vector to a file given by fileName.
      Terminates each string with std::endl, thus, this is intended for 
      text file creation.
      \param fileName Name of file to write.
      \param src Vector of strings to write.
    */
  template <class T> void
  Basic3DConversion<T>::IterativeWrite(wxFileName fileName, std::vector<std::string> src)
  {
    if (fileName.GetName() != _T("error")) {
      wxFileName::Mkdir(fileName.GetPath(wxPATH_GET_VOLUME), 0777, wxPATH_MKDIR_FULL);

      // Test possible overwrite condition.
      if (fileName.FileExists()) {
        std::string errorMsg = "Warning: File exists, will overwrite ";
        errorMsg += fileName.GetFullPath();
        wxLogError(wxFormatString(errorMsg));
        std::cout << errorMsg << std::endl;
      }

      std::ofstream output;
      output.open((const char *) fileName.GetFullPath());

      std::vector<std::string>::iterator it = src.begin();
      std::vector<std::string>::iterator it_end = src.end();
      for (; it < it_end; ++it) {
        output << *it << std::endl;
      } 
      output.close();
    }
    else {
      wxLogError(_T("File name error"));
    }
  }

} // end namespace jcs

#endif
