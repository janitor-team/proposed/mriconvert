/// ImageView.h
/**
*/

#ifndef IMAGE_VIEW_H
#define IMAGE_VIEW_H

#include <wx/wx.h>
#include <wx/image.h>
#include <vector>
#include <string>

class ImageViewerBase;

class BitmapWindow : public wxWindow
{
public:
	BitmapWindow(ImageViewerBase* parent, const wxBitmap& bitmap);

	void SetBitmap(const wxBitmap& bitmap);

	void OnPaint(wxPaintEvent& event);
	void OnErase(wxEraseEvent& event);
	void OnMouseDown(wxMouseEvent& event);
	void OnMouseMotion(wxMouseEvent& event);
	void OnDoubleClick(wxMouseEvent& event);

private:
	DECLARE_EVENT_TABLE()

	wxBitmap mBitmap;
	wxPoint mPoint;
	ImageViewerBase* myParent;

};

class ImageViewerBase: public wxFrame  //Dialog
{
public:
	ImageViewerBase(wxWindow* parent, const wxString& title);
	virtual void UpdateImage() = 0;

protected:
	void OnSlide(wxScrollEvent& event);
	void OnPlay(wxCommandEvent& event);
	void OnTimer(wxTimerEvent& event);
	void OnContextMenu(wxContextMenuEvent& event);
	void OnSave(wxCommandEvent& event);
	void OnSaveAll(wxCommandEvent& event);
	void SetImage(wxImage image);
	virtual void OnWindow(wxCommandEvent& event) {}
	void OnReset(wxCommandEvent& event);
	void OnClose(wxCommandEvent& event);
	BitmapWindow* mpBitmap;
	wxSlider* mpSliceSlider;

private:
	DECLARE_EVENT_TABLE()

	wxButton* mpPlayButton;
	wxStaticText* mpText;
	wxTimer mTimer;
	wxImage mImage;

};


class ImageViewer: public ImageViewerBase
{
public:
	ImageViewer(wxWindow* parent, const std::vector<wxString>& filenames,
		const wxString& show_first);

protected:
	void UpdateImage();
	void OnWindow(wxCommandEvent& event);

private:
	std::vector<wxString> filenames;
	wxString showFirst;

};


class MultiFrameImageViewer: public ImageViewerBase
{
public:
	MultiFrameImageViewer(wxWindow* parent, const wxString& filename);

protected:
	void UpdateImage();

private:
	wxImage GetImage(int frame);
	bool CreateVolume(jcs::DicomFile& file);
	std::vector< std::vector<wxUint8> > volume;
	bool goodVolume;
	int columns;
	int rows;
	int frames;

};

#endif
