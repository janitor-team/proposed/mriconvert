/// NiftiVolume.h
/**
*/

#ifndef NIFTI_VOLUME_H_
#define NIFTI_VOLUME_H_

#include <fstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include <math.h>

#include <wx/defs.h>
#include <wx/log.h>

#include "StringConvert.h"
#include "Globals.h"
#include "Volume.h"
#include "BasicVolumeFormat.h"

// nifti.h must be included last to avoid clash with dirent.h!
#include "nifti1.h"

namespace jcs {

  struct NiftiHeader:public Basic3DHeader {
    NiftiHeader() { InitHeader(); }

    nifti_1_header hdr;

    nifti1_extender extender;

    std::vector<nifti1_extension> extended_header;

    virtual void SetNumberOfSlices(int slices) { hdr.dim[3] = slices; }
    virtual void SetSliceSpacing(double spacing) 
    { hdr.pixdim[3] = static_cast<float>(spacing); }
    virtual int GetNumberOfSlices() { return hdr.dim[3]; }

    void SetDimInfo(char freq_dim, char phase_dim, char slice_dim);

    void InitHeader();
  };


  class NiftiVolume: public BasicVolumeFormat {
    public :
    NiftiVolume(const char* filename, 
    const char* header_extension,
    const char* raw_extension);
    ~NiftiVolume();
    
    NiftiHeader GetHeader()	{ return mHeader; } 

    void WriteHeader(Basic3DHeader* header); 

    private :
    NiftiHeader mHeader;
    void mInitHeader()	{ mHeader.InitHeader(); }
    
    enum aEndianType { aLITTLE_ENDIAN, aBIG_ENDIAN } ;
    aEndianType mByteOrder;

    int  mWriteHeaderFile();
  };

}

#endif
