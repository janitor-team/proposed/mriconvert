/// AnalyzeOptionsDlg.cpp
/**
*/

#include <string>
#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>
#include <wx/dialog.h>
#include <wx/panel.h>

#include "AnalyzeOptionsDlg.h"
#include "AnalyzeOutputter.h"

using namespace jcs;

BEGIN_EVENT_TABLE(AnalyzeOptionsDlg, BasicOptionsDlg)

EVT_BUTTON(wxID_OK, AnalyzeOptionsDlg::OnOkay)

END_EVENT_TABLE()

AnalyzeOptionsDlg::AnalyzeOptionsDlg(AnalyzeOutputter* outputter)
: BasicOptionsDlg(_("Analyze 7.5 options"), outputter), 
  mOutputter(outputter)
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dlgSizer->Add(myPanel, 0, wxEXPAND);

  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  wxButton* okayButton = new wxButton(this, wxID_OK, _("Okay"));
  buttonSizer->Add(okayButton, 0, wxRIGHT, 10);
  buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));
  okayButton->SetDefault();

  dlgSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);

  mNeedsRebuild = false;
}


void
AnalyzeOptionsDlg::OnOkay(wxCommandEvent& event)
{
  bool nfSave = SaveNameFields();
  bool rbld = Rebuild();
  mNeedsRebuild = nfSave || rbld;
  BasicOptionsDlg::OnOkay(event);
  EndModal(event.GetId());
}
