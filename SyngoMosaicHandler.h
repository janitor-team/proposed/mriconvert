/// SyngoMosaicHandler.h
/**
*/

#ifndef SYNGO_MOSAIC_HANDLER_H
#define SYNGO_MOSAIC_HANDLER_H

#include <wx/filename.h>
#include <wx/stdpaths.h>

#include <algorithm>
#include <set>
#include <math.h>

#include "Dictionary.h"
#include "DicomFile.h"
#include "StringConvert.h"
#include "Volume.h"
#include "SyngoHandler.h"

namespace jcs {
  class SyngoMosaicHandler : public SyngoHandler
  {
    public :
    SyngoMosaicHandler(const std::string& seriesUid);

    virtual int GetNumberOfSlices() const;
    virtual int GetRows();
    virtual int GetColumns();
    virtual std::vector<std::string> GetStringInfo();
    virtual bool IsMosaic() const { return true; }

    virtual void GetVolumes (std::map <VolId, Volume <wxInt64> >& v) { mGetVolumes <wxInt64> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxUint64> >& v) { mGetVolumes <wxUint64> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxInt32> >& v) { mGetVolumes <wxInt32> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxUint32> >& v) { mGetVolumes <wxUint32> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxInt16> >& v) { mGetVolumes <wxInt16> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxUint16> >& v) { mGetVolumes <wxUint16> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxInt8> >& v) { mGetVolumes <wxInt8> (v); }
    virtual void GetVolumes (std::map <VolId, Volume <wxUint8> >& v) { mGetVolumes <wxUint8> (v); } 

  protected:
    virtual VolListType ReadVolIds(DicomFile& file);

    private :
    double GetPhaseFov() const;
    double GetRoFov() const;
    template <class T> void
    mGetVolumes(std::map<VolId, Volume<T> >& v);

  };
};

#endif
