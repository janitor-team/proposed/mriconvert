/// Converter.h
/**
*/

#ifndef CONVERTER_H_
#define CONVERTER_H_

#include <set>
#include <string>
#include <map>

#include <wx/string.h>

#include "SeriesHandler.h"
#include "OutputterBase.h"

namespace jcs {

typedef std::vector<wxString> Message;
typedef std::vector<Message> MessageList;


class Converter {

private :
	typedef std::vector<std::string> StringVector;
	typedef std::map<std::string, std::string> StringMap;
	typedef std::map<std::string, SeriesHandler*> HandlerMap;

public :
	Converter() {}
	Converter(int type);
	~Converter();

	OutputList* GetOutputList() { return &mpOutputter->mOutputList; }
	OutputterBase* GetOutputter() { return mpOutputter; }
	const StringVector GetStringInfo(const std::string& s) const;
	const StringVector GetSeriesList();
	SeriesHandler* GetHandler(const std::string& s) { return mHandlerMap[s]; }
	void SetOutput(int type);
	
	int AddFile(const wxString& path, const wxString& match = _T(""));
	void UpdateOutput();
	void UpdateAllOutput();
	void UpdateOutput(const std::string& seriesUid);

	void RemoveSeries(const std::string& seriesUid);

	void Cancel() { mCancel = true; }
	bool IsCancelled() { return mCancel; }
	void ConvertAll();

	MessageList messages;

private :
	bool mAddFile(const wxString& path, const wxString& match, wxString& message);

	// Indexed by SeriesUID.
	HandlerMap mHandlerMap;
	bool mCancel;
	OutputterBase* mpOutputter;
	std::set<std::string> mSeriesToUpdate;

};

}

#endif
