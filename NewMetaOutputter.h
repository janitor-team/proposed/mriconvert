/// NewMetaOutputter.h
/**
*/

#ifndef NEW_META_OUTPUTTER_H_
#define NEW_META_OUTPUTTER_H_

#include <string>
#include <map>

#include "Basic3DOutputter.h"
#include "BasicVolumeFormat.h"

namespace jcs {

  struct NewMetaHeader;

  class NewMetaOutputter : public Basic3DOutputter {

    public :
    NewMetaOutputter();
    virtual int ConvertSeries(SeriesHandler* handler);
    
    bool SaveHeaderOnly() const {
      return saveHeaderOnly;
    }
    void SetSaveHeader(bool value);
    std::vector<std::string> fields;

    void SetOption(const std::string& name, bool value);
    virtual void UpdateOutputForSeries(SeriesHandler* handler);

  protected:
    virtual BasicVolumeFormat* GetOutputVolume(const char* file);

  private:
    static Options CreateOptions();
    bool saveHeaderOnly;

  };

  template <class T>
  class MetaConversion: public Basic3DConversion <T> {

  public:
    MetaConversion(Basic3DOutputter* outputter, SeriesHandler* handler);
    ~MetaConversion();

    NewMetaHeader* GetHeader() {
      return mHeader;
    }

    void Convert();

  protected:
    virtual void GetHeaderForSeries();
    virtual void CompleteHeaderForVolume(std::pair<VolId, Volume<T> > volPair);

  private:
    NewMetaHeader* mHeader;

  };

  
  /**
  template <class T> void
  MetaConversion<T>::WriteGradientFiles()
  {

    GradientInfo info = this->mHandler->GetGradientInfo();

    Normalize(info);

    if (info.values.size() == 0) { return; }

    std::string key = this->mHandler->GetSeriesUid() + this->mOutputter->bvals_postfix;

    wxFileName fileName = this->mOutputter->mOutputList.GetFullFileName(key);

    if (fileName.GetName() == _T("error")) {
      wxLogError(_T("File name error"));
      return;
    }
    wxFileName::Mkdir(fileName.GetPath(wxPATH_GET_VOLUME), 0777, wxPATH_MKDIR_FULL);

    std::ofstream output;

    output.open(fileName.GetFullPath());  //.mb_str(wxConvLocal));
    for (std::vector<double>::iterator it = info.values.begin(); it != info.values.end(); ++it) {
      output << *it << " ";
    }
    output << std::endl;
    output.close();

    key = this->mHandler->GetSeriesUid() + this->mOutputter->bvecs_postfix;
    fileName = this->mOutputter->mOutputList.GetFullFileName(key);

    if (fileName.GetName() == _T("error")) {
      wxLogError(_T("File name error"));
      return;
    }

    output.open(fileName.GetFullPath());  //.mb_str(wxConvLocal));
    {
      std::vector<double>::iterator it = info.xGrads.begin();
      std::vector<double>::iterator it_end != info.xGrads.end();
      for (; it != it_end; ++it) {
        output << *it << " ";
      }
      output << std::endl;
    }
    {
      std::vector<double>::iterator it = info.yGrads.begin();
      std::vector<double>::iterator it_end != info.yGrads.end();
      for (; it != it_end; ++it) {
        if (*it == 0) {
          output << (*it) << " ";
        }
        else {
          output << *it << " ";
        }
      }
      output << std::endl;
    }
    for (std::vector<double>::iterator it = info.zGrads.begin(); it != info.zGrads.end(); ++it) {
    output << *it << " ";
    }
    output << std::endl;

    output.close();

  }
  **/

}

#endif

