// fileformats.h
// Note explicit newlines and line continuation characters,
// required to allow proper interaction with wxTextCtrl.

std::string fileformats_h = 
"About medical image coordinate systems \n \n \
 \n \
Medical image data can be (and is) stored in a variety of ways. In order to orient a two-dimensional image or three-dimensional volume in space, one needs to know how the order of the data on disk relates to a standard set of axes. For medical imaging, these axes are generally chosen to correspond to the directions left/right, posterior/anterior, and superior/inferior.  \n \
 \n \
 \n \
DICOM \n \
 \n \
The DICOM standard uses a patient-centric LPS, coordinate system, where +x = left, +y = posterior, and +z = superior. The direction cosines between the rows and columns of the image and these axes are stored in the Image Orientation Patient field. For axial images with no rotation, this field will be 1/0/0/0/1/0.  \
This may be read as \"image rows increase from right to left, image columns increase from anterior to posterior\". Most DICOM viewers follow the convention of plotting the data starting in the upper left corner of the view. For axial brain images, this will place the left side of the brain on the right side of the view, and the posterior of the brain toward the bottom of the view. This point of view is sometimes refered to as \"radiological\". \
 \n \n \
Sagital images will have an Image Orientation Patient field of 0/1/0/0/0/-1, indicating that rows increase from anterior to posterior, and columns increase from superior to inferior. In this case, the posterior will be toward the right, and the inferior toward the bottom (as one well might wish). Coronal images will have an Image Orientation Patient field of 1/0/0/0/0/-1. \n \
 \n \
MRIConvert uses the Image Orientation Patient field to calculate the normal to the image plane, and the Image Position Patient to determine the position of each slice along the normal. This is the only method of ordering slices that will always work with any DICOM-compliant files. \n \
 \n \
Only the NIfTI output formats preserve the full orientation information. For BrainVoyager and the Analyze variants, the data is assumed to represent axial slices, and the order of the data is changed to match the standard orientation of these formats. For Metaimage output, no assumptions are made about data order, and no orientation information is stored. \n \
 \n \
 \n \
Analyze 7.5 \n \
 \n \
Analyze 7.5 files are output in the LAS coordinate system. The image data is flipped in the column direction before writing. Currently, the orient field is always set to the value 0, indicating transverse unflipped data, regardless of whether the data represents transverse, sagital, coronal, or oblique slices. \n \
 \n \
Most Analyze viewers plot image data starting from the lower left corner of the view. For \"LA\" axial images, this results in a \"radiological\" view identical to the original DICOM view. Note the \"LAS\" is a left-handed coordinate system. Left-handed data ordering is sometimes refered to as \"radiological\" ordering, and right-handed ordering as \"neurological\". Those terms can be misleading and should probably be restricted to their original context: describing the appearance of two-dimensional transverse brain images. \n \
 \n \
 \n \
SPM99/Analyze \n \
 \n \
The SPM99/Analyze output of MRIConvert is similar to the Analyze 7.5 output, with a few important distinctions. In the header, the \"originator\" field, left blank for Analyze 7.5, is now defined as the \"origin\" field. MRIConvert sets this to the middle of the volume. More importantly, the image data is rotated 180 degrees before writing. This means that transverse slices are stored RAS. \n \
 \n \
 \n \
NIfTI \n \
 \n \
NIfTI output is based on the most straightforward conversion of DICOM to NIfTI according to the NIfTI standard. Image data is not rewritten, and the image orientation relative to NIfTI's standard RAS axes is given by the qform fields. You can expect axial images to be stored LPS. How they are displayed and interpreted will (as always) depend entirely on the software you use to read them, its assumption about data order, and its support (or lack thereof) for the qform information. Any software fully compliant with the NIfTI standard should be able to read these files and their orientations. \n \
 \n \
 \n \
FSL/NIfTI \n \
 \n \
This is an output format specifically designed for use with FSL. It should be emphasized that this is a fully compliant NIfTI file, and not an FSL-specific variant. However, data order and some header fields have been modified to make use with FSL a bit smoother. Data is flipped top bottom, as in the Analyze 7.5 output, to avoid problems with FSL's coordinate system. This will also result in a more intuitive view in FSLView and other FSL-generated reports. All transformations are accounted for in the qform fields. Also, the data type for unsigned 16 bit data has been changed to signed 16 bit, as FSL does not yet support the unsigned 16 bit data type. This should not be a problem for MRI data, as it uses only the lower 12 bits. Also, for Siemen's diffusion images, bvals and bvecs files will be created automically. The values in these files will reflect any rotation of the image volume relative to the gradient coordinates, and will be correspond to the order of the data on disk. \n \
 \n \
";
