/// DicomElement.h
/**
*/

#ifndef DICOMELEMENT_H_
#define DICOMELEMENT_H_

#include <map>
#include <vector>
#include <string>
#include <istream>
#include <iostream>

#include <wx/defs.h>

#include "DicomTag.h"

namespace jcs {
  ///
  /** Provides the relationship between a DICOM tag and text 
      description of the tag.
  */
  struct DicomElement {
    
    DicomElement() {}
    DicomElement(const DicomTag& tag,
    const char* desc = "", 
    const char* vr = "UN") :
    tag(tag), description(desc), vr(vr) {}
    /// The DICOM group and element numbers.
    DicomTag tag;
    /// The DICOM field name.
    std::string description;
    /// The DICOM field data type identifier.
    std::string vr;
    std::string value;
    wxUint32 value_length;

  };
}

#endif
