/// AnalyzeOutputter.cpp
/** Encapsulates the logic of the Analyze format.
*/

#include <wx/string.h>
#include <wx/log.h>

#include <string>
#include <vector>

#include "SeriesHandler.h"
#include "AnalyzeVolume.h"
#include "AnalyzeOutputter.h"

using namespace jcs;

///
/**
 */
AnalyzeOutputter::AnalyzeOutputter() :
Basic3DOutputter(CreateOptions())
{
}


///
/**
 */
Options
AnalyzeOutputter::CreateOptions()
{
  Options options = Get3DOptions();
  options.pathname = "Analyze";
  return options;
}


///
/**
 */
BasicVolumeFormat* 
AnalyzeOutputter::GetOutputVolume(const char* file)
{
  //return new AnalyzeVolume(file, headerExtension.mb_str(wxConvLocal), rawExtension.mb_str(wxConvLocal));
  return new AnalyzeVolume(file, headerExtension, rawExtension);
}


///
/** Creates an instance of AnalyzeConversion capable of 
    handling the incoming DICOM pixel data.
    \param handler A pointer to the SeriesHandler.
    \return 1
*/
int
AnalyzeOutputter::ConvertSeries(SeriesHandler* handler)
{
  int bits_allocated, pixel_rep = 0;
  handler->Find("BitsAllocated", bits_allocated);
  handler->Find("PixelRepresentation", pixel_rep);

  switch (bits_allocated + pixel_rep) {

  case 8 : {
      AnalyzeConversion<wxUint8> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 9 : {
      AnalyzeConversion<wxInt8> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 16 : {
      AnalyzeConversion<wxUint16> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 17 : {
      AnalyzeConversion<wxInt16> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 32 : {
      AnalyzeConversion<wxUint32> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 33 : {
      AnalyzeConversion<wxInt32> conversion(this, handler);
      conversion.Convert();
      break;
    }
  default : {
      wxLogMessage(_("BitsAllocated and PixelRepresentation values (%d, %d) not supported."), bits_allocated, pixel_rep);
    }
  }
  return 1;
}


///
/**
*/
template <class T>
AnalyzeConversion<T>::AnalyzeConversion(Basic3DOutputter* outputter, SeriesHandler* handler)
: Basic3DConversion<T>(outputter, handler)
{
  mHeader = new AnalyzeHeader();
}


///
/**
*/
template <class T>
AnalyzeConversion<T>::~AnalyzeConversion()
{
  delete mHeader;
}


///
/**
*/
template <class T> void
AnalyzeConversion<T>::ProcessSlice(std::vector<T>& slice)
{
  // rotate 180
  reverse(slice.begin(), slice.end());

  // flip L/R
  unsigned int row_size = mHeader->dime.dim[1];
  if (this->mHeader->dime.datatype == 128) { //rgb
    row_size *= 3;
  }
  typename std::vector<T>::iterator row_begin = slice.begin();

  while (row_begin < slice.end()) {
    typename std::vector<T>::iterator row_end = row_begin + row_size;
    reverse(row_begin, row_end);
    row_begin = row_end;
  }
}


///
/**
*/
template <class T> void
AnalyzeConversion<T>::GetHeaderForSeries()
{

  mHeader->InitHeader();
  std::string s;
  this->mHandler->Find("SeriesDate", s);
  
  s.copy(mHeader->hist.exp_date, sizeof(mHeader->hist.exp_date));
  
  this->mHandler->Find("SeriesTime", s);
  s.copy(mHeader->hist.exp_time, sizeof(mHeader->hist.exp_time));
  
  this->mHandler->Find("SeriesDescription", s);
  s.copy(mHeader->hist.descrip, sizeof(mHeader->hist.descrip));

  this->mHandler->Find("PatientName", s);
  s.copy(mHeader->hist.patient_id, sizeof(mHeader->hist.patient_id));

  float fvar;
  this->mHandler->Find("RepetitionTime", fvar);
  mHeader->dime.pixdim[4] = fvar;

  mHeader->dime.dim[1] = this->mHandler->GetColumns();
  mHeader->dime.dim[2] = this->mHandler->GetRows();
  mHeader->dime.dim[3] = this->mHandler->GetNumberOfSlices();
  // dim[3] recalculated in mConvert

  std::vector<double>voxel_size = this->mHandler->GetVoxelSize();
  mHeader->dime.pixdim[1] = static_cast<float>(voxel_size[0]);
  mHeader->dime.pixdim[2] = static_cast<float>(voxel_size[1]);
  mHeader->dime.pixdim[3] = static_cast<float>(voxel_size[2]);
  // pixdim[3] recalculated in mConvert

  this->mHandler->Find("PhotometricInterpretation", s);
  if (s == "RGB") {
    mHeader->dime.datatype = 128; // DT_RGB
  }
  else {

    this->mHandler->Find("BitsAllocated", mHeader->dime.bitpix);

    // Standard Analyze data types don't include
    // either unsigned 16 bit or signed 8 bit
    switch (mHeader->dime.bitpix) {
    case 16 : 
      mHeader->dime.datatype = 4;	// DT_SIGNED_SHORT 
      break;

    case 8 :
      mHeader->dime.datatype = 8;  // DT_UNSIGNED_CHAR
      break;

      default :
      mHeader->dime.datatype = 0;  // DT_UNKNOWN
    }
  }

  // Write/calculate remaining fields
  mHeader->hk.sizeof_hdr = 348;
  mHeader->dime.dim[0] = 4;
  mHeader->dime.dim[4] = 1; // one volume per file by default
  int dimensionality = this->mOutputter->GetDimensionality(this->mHandler->GetSeriesUid());
  if (dimensionality == 4) {
    mHeader->dime.dim[4] =  this->GetNumberOfVolumes();
  }
  mHeader->dime.vox_offset = 0;
  mHeader->hk.regular = 'r'; // all volumes same size
}


///
/**
*/
template <class T> void
AnalyzeConversion<T>::CompleteHeaderForVolume(std::pair<VolId, Volume<T> > volPair)
{
  // Calculate number of slices for this volume
  int n_slices = volPair.second.size();
  mHeader->SetNumberOfSlices(n_slices);
  
  // Calculate voxel size in Z from slice spacing for multi-slice files
  if (n_slices > 1) {
    mHeader->SetSliceSpacing(volPair.second.GetSpacing());
  }
}
