/// SpmOptionsDlg.h
/**
  */

#ifndef SPM_OPTIONS_DLG_H_
#define SPM_OPTIONS_DLG_H_

#include "BasicOptionsDlg.h"

namespace jcs {

  class NewSpmOutputter;
  
  class SpmOptionsDlg : public BasicOptionsDlg
  {
  public:
    SpmOptionsDlg(NewSpmOutputter* outputter);

    void OnOkay(wxCommandEvent& event);
    bool NeedsRebuild() const { return mNeedsRebuild; }

  protected:
    DECLARE_EVENT_TABLE()

      private:
    bool mNeedsRebuild;
    bool mSplit;
    int mDim;
    int mSkip;
    NewSpmOutputter* mOutputter;
  };

}

#endif
