/// DicomElementInstance.h
/** This class represents a DICOM Data Element
*/

#ifndef DICOMELEMENTINSTANCE_H_
#define DICOMELEMENTINSTANCE_H_

#include <map>
#include <vector>
#include <string>
#include <istream>
#include <iostream>

#include <wx/defs.h>

#include "DicomTag.h"
#include "Dictionary.h"
#include "ValueRepresentations.h"

namespace jcs {
  ///
  /** Provides the relationship between a DICOM tag and text 
      description of the tag.
  */
  class DicomElementInstance {
    public:
    DicomElementInstance() {}
    DicomElementInstance(Dictionary *dictionary) :
      mDicomDictionary(dictionary) {}
    DicomElementInstance(wxUint8 code, Dictionary *dictionary) :
      transferSyntaxCode(code), mDicomDictionary(dictionary) {}
    DicomElementInstance(const DicomTag &tag,
      const char *desc = "", 
      const char *vr = "UN") :
      tag(tag), description(desc), vr(vr) {}
    wxUint8 transferSyntaxCode;
    /// The DICOM group and element numbers.
    DicomTag tag;
    /// The DICOM field name.
    std::string description;
    /// The DICOM field data type identifier.
    std::string vr;
    // Consider using a char pointer with dynamic memory
    // allocation, freeing when done.
    char value_buff[1024];
    char vr_buff[3];
    wxUint32 value_length;
    DicomValue *myValue;
    Dictionary *mDicomDictionary;
    bool IsExplicitVR();
    std::string GetVR();
    wxUint32 EndianSwapUint32(wxUint32 value);
    friend std::istream &operator>>(std::istream &in, DicomElementInstance &de);
    friend std::ostream &operator<<(std::ostream &out, DicomElementInstance &de);
  };

  std::istream &operator>>(std::istream &in, DicomElementInstance &de);
  std::ostream &operator>>(std::ostream &out, DicomElementInstance &de);
  
}

#endif
