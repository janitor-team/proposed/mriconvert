#ifndef INFO_FRAME_H
#define INFO_FRAME_H

#include <vector>
#include <string>

#include <wx/wx.h>
#include <wx/frame.h>

class wxTextCtrl;

class InfoFrame : public wxFrame
{
public:
    InfoFrame(wxWindow* parent, std::vector<std::string> info);

private:
	int AddString(std::string& s);

	wxTextCtrl* mTextCtrl;

};

#endif

