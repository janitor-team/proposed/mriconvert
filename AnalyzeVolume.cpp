/// AnalyzeVolume.cpp
/**
*/

#if defined(__WXGTK__) || defined(__WXMOTIF__)
  #include <wx/wx.h>
#endif

#include <wx/log.h>

#include <iostream>
#include <algorithm>

#include "AnalyzeVolume.h"
#include "StringConvert.h"

using namespace jcs;

///
/**
*/
AnalyzeVolume::AnalyzeVolume(const char* filename, 
const char* he, const char* re) 
: BasicVolumeFormat(filename, he, re), mByteOrder(aLITTLE_ENDIAN)
{
  mInitHeader();
}


///
/**
*/
AnalyzeVolume::~AnalyzeVolume()
{
}


///
/**
*/
void
AnalyzeVolume::WriteHeader(Basic3DHeader* header)
{ 
  AnalyzeHeader* spm_header = dynamic_cast<AnalyzeHeader*>(header);
  mHeader = *spm_header;
  if (mWriteHeaderFile() == 1) {
    if (verbose && !quiet) {
      std::cout << "Wrote " << mFileName.GetFullPath() << std::endl;
    }
  }
}


///
/**
*/
int
AnalyzeVolume::mWriteHeaderFile() 
{
  if (!mOpenHeaderFile(std::ios::out|std::ios::binary)) {
    wxLogError(_T("Cannot create header file %s"),
    mFileName.GetFullName().c_str());
    return 0;
  }

  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.sizeof_hdr), sizeof(mHeader.hk.sizeof_hdr));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.data_type), sizeof(mHeader.hk.data_type));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.db_name), sizeof(mHeader.hk.db_name));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.extents), sizeof(mHeader.hk.extents));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.session_error), sizeof(mHeader.hk.session_error));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.regular), sizeof(mHeader.hk.regular));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hk.hkey_un0), sizeof(mHeader.hk.hkey_un0));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.dim), sizeof(mHeader.dime.dim));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused8), sizeof(mHeader.dime.unused8));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused9), sizeof(mHeader.dime.unused9));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused10), sizeof(mHeader.dime.unused10));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused11), sizeof(mHeader.dime.unused11));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused12), sizeof(mHeader.dime.unused12));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused13), sizeof(mHeader.dime.unused13));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.unused14), sizeof(mHeader.dime.unused14));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.datatype), sizeof(mHeader.dime.datatype));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.bitpix), sizeof(mHeader.dime.bitpix));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.dim_un0), sizeof(mHeader.dime.dim_un0));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.pixdim), sizeof(mHeader.dime.pixdim));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.vox_offset), sizeof(mHeader.dime.vox_offset));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.funused1), sizeof(mHeader.dime.funused1));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.funused2), sizeof(mHeader.dime.funused2));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.funused3), sizeof(mHeader.dime.funused3));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.cal_max), sizeof(mHeader.dime.cal_max));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.cal_min), sizeof(mHeader.dime.cal_min));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.compressed), sizeof(mHeader.dime.compressed));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.verified), sizeof(mHeader.dime.verified));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.glmax), sizeof(mHeader.dime.glmax));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.dime.glmin), sizeof(mHeader.dime.glmin));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.descrip), sizeof(mHeader.hist.descrip));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.aux_file), sizeof(mHeader.hist.aux_file));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.orient), sizeof(mHeader.hist.orient));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.originator), sizeof(mHeader.hist.originator));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.generated), sizeof(mHeader.hist.generated));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.scannum), sizeof(mHeader.hist.scannum));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.patient_id), sizeof(mHeader.hist.patient_id));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.exp_date), sizeof(mHeader.hist.exp_date));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.exp_time), sizeof(mHeader.hist.exp_time));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.hist_un0), sizeof(mHeader.hist.hist_un0));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.views), sizeof(mHeader.hist.views));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.vols_added), sizeof(mHeader.hist.vols_added));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.start_field), sizeof(mHeader.hist.start_field));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.field_skip), sizeof(mHeader.hist.field_skip));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.omax), sizeof(mHeader.hist.omax));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.omin), sizeof(mHeader.hist.omin));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.smax), sizeof(mHeader.hist.smax));
  mHeaderFile.write(reinterpret_cast <const char*> (&mHeader.hist.smin), sizeof(mHeader.hist.smin));

  mCloseHeaderFile();

  return 1;
}


///
/**
*/
void 
AnalyzeVolume::mInitHeader()
{ 
  mHeader.InitHeader(); 
}


///
/**
*/
void
AnalyzeHeader::InitHeader()
{ 
  memset(&hist, 0, sizeof(hist));
  memset(&dime, 0, sizeof(dime));
  memset(&hk, 0, sizeof(hk));
}
