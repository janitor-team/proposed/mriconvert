/// AnalyzeOutputter.h
/** Oversees the conversion and output production of 
    the Analyze format.
*/

#ifndef ANALYZE_OUTPUTTER_H_
#define ANALYZE_OUTPUTTER_H_

#include <string>
#include <map>

#include "Basic3DOutputter.h"
#include "BasicVolumeFormat.h"

namespace jcs {

  struct AnalyzeHeader;

  class AnalyzeOutputter : public Basic3DOutputter {

    public :
    AnalyzeOutputter();

    virtual int ConvertSeries(SeriesHandler* handler);

  protected:
    virtual BasicVolumeFormat* GetOutputVolume(const char* file);

  private:
    static Options CreateOptions();
  };


  template <class T>
  class AnalyzeConversion: public Basic3DConversion <T> {

  public:
    AnalyzeConversion(Basic3DOutputter* outputter, SeriesHandler* handler);
    ~AnalyzeConversion();

  protected:
    virtual void GetHeaderForSeries();
    virtual void CompleteHeaderForVolume(std::pair<VolId, Volume<T> > volPair);
    virtual void ProcessSlice(std::vector<T>& slice);

    AnalyzeHeader* GetHeader() { return mHeader; }

  private:
    AnalyzeHeader* mHeader;

  };

}

#endif
